package com.example.test.controllers;

import com.example.test.entities.Client;
import com.example.test.services.ClientServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientController {

    @Autowired
    private ClientServiceImpl clientService;

    @RequestMapping("/client")
    public ResponseEntity<Client> getClient(long id){
        try {
            return new ResponseEntity<>(clientService.getClientById(id), HttpStatus.OK);
        }catch (NullPointerException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @RequestMapping("/clientlist")
    public ResponseEntity<List<Client>> getClients(){
        try {
            return new ResponseEntity<>(clientService.getList(), HttpStatus.OK);
        }catch (NullPointerException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping("/deleteclient")
    public String deleteClient(long id){
        String message;
        try {
            clientService.deleteClient(id);
            message = "Successful " + HttpStatus.OK;
            return message;
        }catch (NullPointerException e){
            e.printStackTrace();
            message = "Error ";
            return message + HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
    @RequestMapping("/")
    public String updateClient(Client client){
        String message;
        try {
            clientService.updateClient(client); //It exists add and update methods
            message = "Successful " + HttpStatus.OK;
            return message;
        }catch (NullPointerException e){
            e.printStackTrace();
            message = "Error ";
            return message + HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
