package com.example.test.controllers;

import com.example.test.authorization.Logining;
import com.example.test.authorization.Registration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AuthorizationController {
    
    @Autowired
    private Logining logining;
    @Autowired
    private Registration registration;

    @RequestMapping("/authorization")
    public boolean authorization(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password) {
        if(logining.isLogin(login, password)) {
            return true;
        }
        else if (!logining.isLogin(login, password)) {
            registration.getRegistration(login, password);
            return true;
        }
        return false;
    }
}
