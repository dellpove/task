package com.example.test.repositories;

import com.example.test.entities.Token;
import org.springframework.data.repository.CrudRepository;

public interface TokenRepository extends CrudRepository<Token,Long> {
}
