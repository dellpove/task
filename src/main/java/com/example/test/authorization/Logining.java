package com.example.test.authorization;

import com.example.test.entities.Token;
import com.example.test.repositories.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Logining {

    @Autowired
    private TokenRepository tokenRepository;

    public boolean isLogin(String login, String password){
        List<Token> tokens = (List<Token>) tokenRepository.findAll();
        for(Token temp: tokens){
            if(temp.getLogin()==login && temp.getPassword()==password){
                return true;
            }
        }
        return false;
    }
}
