package com.example.test.authorization;

import com.example.test.entities.Token;
import com.example.test.repositories.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class Registration {
    @Autowired
    private TokenRepository tokenRepository;
    private Token token = new Token();

    public void getRegistration(String nickname, String password) {
        token.setLogin(nickname);
        token.setPassword(password);
        token.setToken(Integer.parseInt(this.getToken(nickname,password)));
        tokenRepository.save(token);
    }
    public String getToken(String nickname, String password){
        String generatedPassword = nickname + password;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(generatedPassword.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

}
