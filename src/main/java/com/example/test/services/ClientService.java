package com.example.test.services;

import com.example.test.entities.Client;
import java.util.List;

public interface ClientService {
    Client getClientById(long id);
    List<Client> getList();
    void deleteClient(long id);
    void updateClient(Client client);
    void saveClient(Client client);
}
