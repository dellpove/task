package com.example.test.services;

import com.example.test.entities.Client;
import com.example.test.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl {
    @Autowired
    private ClientRepository clientRepository;

    public Client getClientById(long id){
        Client client = clientRepository.findOne(id);
        if(client != null) {
            return client;
        }else{
            throw new NullPointerException("Client not found");
        }
    }

    public List<Client> getList(){
        List<Client> list = (List<Client>) clientRepository.findAll();
        if(list.size() != 0)
            return list;
        else
            throw new NullPointerException("Repository is empty");
    }
    public void deleteClient(long id){
        clientRepository.delete(id);
    }
    public void updateClient(Client client){
        clientRepository.save(client);
    }
}
