package com.example.test.entities;

import javax.persistence.*;

@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "ClientFirstName")
    private String clientFirstName;
    @Column(name = "ClientLastName")
    private String clientLastName;

    public Client(String clientFirstName, String clientLastName) {
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
    }

    public Client() {
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public long getId() {
        return id;
    }
}
